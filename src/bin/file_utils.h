#ifndef FILE_UTILS_H
#define FILE_UTILS_H

#include <Eina.h>

char *
file_plain_load(const char *file);

char *
file_load(const char *file);

Eina_Bool
file_plain_save(const char *file, const char *text);

Eina_Bool
file_save(const char *file, const char *text);


#endif
