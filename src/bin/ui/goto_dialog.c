#include "config.h"
#include <Elementary.h>

#include "../Ecrire.h"

typedef struct
{
   Evas_Object   *popup;
   Evas_Object   *entry;
   Ecrire_Editor *inst;
} Goto_Data;

static void
_goto_do(Goto_Data *gd)
{
   Evas_Object *entry, *tb;
   Evas_Textblock_Cursor *mcur;
   int line;

   line = atoi(elm_object_text_get(gd->entry));
   entry = gd->inst->entry;
   tb = elm_entry_textblock_get(entry);
   mcur = evas_object_textblock_cursor_get(tb);

   evas_object_del(gd->popup);

   if (line > 0)
     {
        Evas_Coord x, y;

        evas_textblock_cursor_line_set(mcur, line-1);
        elm_entry_calc_force(entry);
        evas_object_smart_callback_call(entry, "cursor,changed", NULL);
        elm_object_focus_set(entry, EINA_TRUE);
        elm_entry_cursor_geometry_get(entry, &x, &y, NULL, NULL);
        elm_scroller_region_bring_in(entry, 0, y, x, y);
        elm_entry_cursor_line_begin_set(entry);
     }
}

static void
_cb_goto_clicked(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Goto_Data *gd = data;

   _goto_do(gd);
}

static void
_cb_goto_key_down(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Goto_Data *gd;
   Evas_Event_Key_Down *ev;

   gd = data;
   ev = event_info;

   if (!strcmp(ev->keyname, "Return"))
     _goto_do(gd);
   else if (!strcmp(ev->keyname, "Escape"))
     evas_object_del(gd->popup);
}

static void
_cb_popup_del(void *data EINA_UNUSED, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
              void *event_info EINA_UNUSED)
{
   Goto_Data *gd = data;

   free(gd);
}

Evas_Object *
ui_goto_dialog_open(Evas_Object *parent, Ecrire_Editor *inst)
{
   Evas_Object *popup, *hbx, *ent, *btn;
   Goto_Data *gd;

   gd = calloc(1, sizeof(Goto_Data));
   if (!gd) return NULL;

   gd->inst = inst;
   gd->popup = popup = elm_popup_add(parent);
   elm_object_part_text_set(popup, "title,text", _("Go to line..."));
   evas_object_event_callback_add(popup, EVAS_CALLBACK_DEL, _cb_popup_del, gd);

   hbx = elm_box_add(parent);
   elm_box_horizontal_set(hbx, EINA_TRUE);
   evas_object_size_hint_align_set(hbx, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_weight_set(hbx, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_show(hbx);

   gd->entry = ent = elm_entry_add(parent);
   elm_entry_scrollable_set(ent, EINA_TRUE);
   elm_entry_single_line_set(ent, EINA_TRUE);
   elm_entry_editable_set(ent, EINA_TRUE);
   evas_object_size_hint_align_set(ent, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_weight_set(ent, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   elm_box_pack_end(hbx, ent);
   evas_object_show(ent);
   evas_object_event_callback_add(ent, EVAS_CALLBACK_KEY_DOWN, _cb_goto_key_down, gd);

   btn = elm_button_add(parent);
   elm_object_text_set(btn, _("Go"));
   evas_object_size_hint_align_set(btn, EVAS_HINT_FILL, 0.0);
   evas_object_size_hint_weight_set(btn, 0.1, EVAS_HINT_EXPAND);
   elm_box_pack_end(hbx, btn);
   evas_object_show(btn);
   evas_object_smart_callback_add(btn, "clicked", _cb_goto_clicked, gd);

   elm_object_content_set(popup, hbx);
   elm_popup_orient_set(popup, ELM_POPUP_ORIENT_CENTER);
   evas_object_show(popup);

   return popup;
}

