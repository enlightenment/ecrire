#include "config.h"
#include <Elementary.h>

#include "../Ecrire.h"

typedef struct
{
   Evas_Object     *list;
   Evas_Object     *fsize;
   Evas_Object     *dfont_check;
   Elm_Object_Item *cur_font;
} Settings;

static Settings _settings;

static void
_cb_popup_hide(void *data, Evas_Object *obj EINA_UNUSED,
               void *event_info EINA_UNUSED)
{
   Ecrire_Editor *inst = data;

   if (inst->settings_popup)
     evas_object_hide(inst->settings_popup);
   elm_object_focus_set(inst->win, 1);
}

static Eina_List *
_font_list_get(const Evas *e)
{
   Eina_List *fonts = evas_font_available_list(e);
   Eina_List *itr, *nitr;
   const char *font, *prev_font = NULL;

   fonts = eina_list_sort(fonts, eina_list_count(fonts), (Eina_Compare_Cb) strcasecmp);
   EINA_LIST_FOREACH_SAFE(fonts, itr, nitr, font)
     {
        Elm_Font_Properties *efp;

        efp = elm_font_properties_get(font);
        if (!efp) continue;
        /* Remove dups */
        if (prev_font && !strcmp(efp->name, prev_font))
          fonts = eina_list_remove_list(fonts, itr);
        else
          {
             eina_stringshare_replace(&font, efp->name);
             prev_font = font;
             eina_list_data_set(itr, font);
          }
        elm_font_properties_free(efp);
     }

   return fonts;
}

static void
_cb_font_selected(void *data, Evas_Object *obj EINA_UNUSED,
                  void *event_info)
{
   const char *selected;
   Ecrire_Editor *inst;
   Elm_Object_Item *it = event_info;
   if (!it) return;

   inst = data;
   selected = elm_object_item_text_get(it);

   if (elm_check_state_get(_settings.dfont_check))
     return;

   eina_stringshare_replace(&inst->font.name, selected);
   ecrire_editor_font_save(inst, inst->font.name, inst->font.size);
}

static void
_cb_spinner_delay_changed(void *data, Evas_Object *obj EINA_UNUSED,
                          void *event_info)
{
   Ecrire_Editor *inst = data;

   inst->font.size = elm_spinner_value_get(obj);

   if (inst->font.name)
     ecrire_editor_font_save(inst, inst->font.name, inst->font.size);
}

static void
_cb_check_changed(void *data, Evas_Object *obj,
                  void *event_info EINA_UNUSED)
{
   Ecrire_Editor *inst;
   Eina_Bool enabled = elm_check_state_get(obj);

   inst = data;

   elm_object_disabled_set(_settings.list, enabled);
   elm_object_disabled_set(_settings.fsize, enabled);

   if (enabled)
     {
        inst->font.name = NULL;
        ecrire_editor_font_save(inst, NULL, 0);
     }
   else if (inst->font.name)
     {
        ecrire_editor_font_save(inst, inst->font.name, inst->font.size);
     }
}

static void
_cb_btn_clicked(void *data, Evas_Object *obj,
                void *event_info EINA_UNUSED)
{
   Ecrire_Editor *inst = data;
   _cb_popup_hide(inst, NULL, NULL);
}

Evas_Object *
ui_settings_open(Evas_Object *parent, Ecrire_Editor *inst)
{
   Evas_Object *pop, *tb, *rec, *bx, *hbx, *btn, *list, *lb, *sp, *ck;

   if (inst->settings_popup)
     {
        evas_object_show(inst->settings_popup);
        return inst->settings_popup;
     }

   inst->settings_popup = pop = elm_popup_add(parent);
   elm_object_style_set(pop, "transparent");
   evas_object_size_hint_weight_set(pop, 1, 1);
   evas_object_smart_callback_add(pop, "dismissed", _cb_popup_hide, inst);
   elm_object_part_text_set(pop, "title,text", _("Settings"));

   bx = elm_box_add(pop);
   evas_object_size_hint_weight_set(bx, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_show(bx);
   elm_object_content_set(pop, bx);

   tb = elm_table_add(pop);
   evas_object_size_hint_weight_set(tb, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_show(tb);
   elm_box_pack_end(bx, tb);

   rec = evas_object_rectangle_add(evas_object_evas_get(pop));
   evas_object_size_hint_min_set(rec, ELM_SCALE_SIZE(300), ELM_SCALE_SIZE(300));
   elm_table_pack(tb, rec, 0, 0, 1, 1);

   _settings.list = list = elm_list_add(pop);
   evas_object_size_hint_weight_set(list, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(list, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_list_select_mode_set(list, ELM_OBJECT_SELECT_MODE_ALWAYS);
   elm_list_multi_select_set(list, 0);
   evas_object_show(list);
   elm_table_pack(tb, list, 0, 0, 1, 1);

   hbx = elm_box_add(bx);
   elm_box_horizontal_set(hbx, 1);
   elm_box_pack_end(bx, hbx);
   evas_object_show(hbx);

   lb = elm_label_add(hbx);
   elm_object_text_set(lb, _("Font size:"));
   elm_box_pack_end(hbx, lb);
   evas_object_show(lb);

   _settings.fsize = sp = elm_spinner_add(hbx);
   elm_spinner_label_format_set(sp, _("%.0f px"));
   elm_spinner_step_set(sp, 1);
   elm_spinner_wrap_set(sp, 0);
   elm_spinner_min_max_set(sp, 10, 72);
   elm_spinner_value_set(sp, inst->font.size);
   evas_object_size_hint_align_set(sp, 0.0, 0.5);
   elm_box_pack_end(hbx, sp);
   evas_object_show(sp);
   evas_object_smart_callback_add(sp, "delay,changed", _cb_spinner_delay_changed, inst);

   Eina_List *fonts, *l;
   const char *font;

   fonts = _font_list_get(evas_object_evas_get(list));
   EINA_LIST_FOREACH(fonts, l, font)
     {
        Elm_Object_Item *it;
        it = elm_list_item_append(list, font, NULL, NULL, NULL, NULL);
        if (inst->font.name && !strcmp(inst->font.name, font))
          _settings.cur_font = it;
     }
   elm_list_go(list);

   if (_settings.cur_font)
     {
        elm_list_item_bring_in(_settings.cur_font);
        elm_list_item_selected_set(_settings.cur_font, 1);
     }

   EINA_LIST_FREE(fonts, font)
     eina_stringshare_del(font);

   evas_object_smart_callback_add(list, "selected", _cb_font_selected, inst);

   _settings.dfont_check = ck = elm_check_add(pop);
   elm_object_text_set(ck, _("Default Font?"));
   elm_check_state_set(ck, !inst->font.name);
   evas_object_show(ck);
   elm_box_pack_end(hbx, ck);
   evas_object_smart_callback_add(ck, "changed", _cb_check_changed, inst);
   _cb_check_changed(inst, ck, NULL);

   btn = elm_button_add(pop);
   evas_object_size_hint_weight_set(btn, 1.0, 0);
   elm_object_text_set(btn, _("Close"));
   evas_object_show(btn);
   elm_box_pack_end(bx, btn);
   evas_object_smart_callback_add(btn, "clicked", _cb_btn_clicked, inst);

   evas_object_show(pop);

   return pop;
}
