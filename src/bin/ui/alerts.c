#include "config.h"
#include <Elementary.h>

#include "../Ecrire.h"

typedef struct
{
   void (*done_cb)(void *data);
   void *data;

   Ecrire_Editor *inst;
   Evas_Object   *popup;
} Alert_Data;

static void
_alert_del(Alert_Data *ad)
{
   if (ad->popup)
     evas_object_del(ad->popup);
   free(ad);
}

static void
_cb_discard(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Ecrire_Editor *inst;
   Alert_Data *ad = data;

   inst = ad->data;

   ad->done_cb(inst);

   _alert_del(ad);
}

static void
_fs_save_done(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
              void *event_info)
{
   Alert_Data *ad;
   Ecrire_Editor *inst;
   const char *selected = event_info;

   inst = data;
   ad = inst->data;

   if (selected)
     {
        ecrire_editor_save(inst, selected);
        inst->data = NULL;
        ad->done_cb(ad->data);
        _alert_del(ad);
     }
}

static void
_cb_save(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Ecrire_Editor *inst;
   Alert_Data *ad = data;

   inst = ad->data;
   inst->data = ad;

   evas_object_del(ad->popup);
   ad->popup = NULL;

   ecrire_editor_try_save(inst, _fs_save_done);
}

static void
_cb_cancel(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Alert_Data *ad = data;

   _alert_del(ad);
}

void
ui_alert_need_saving(Evas_Object *entry, void (*done_cb)(void *data), void *data)
{
   Evas_Object *popup, *btn;
   Alert_Data *ad;

   ad = calloc(1, sizeof(Alert_Data));
   EINA_SAFETY_ON_NULL_RETURN(ad);

   ad->popup = popup = elm_popup_add(elm_object_top_widget_get(entry));
   ad->done_cb = done_cb;
   ad->data = data;

   elm_object_part_text_set(popup, "title,text", "Unsaved Changes");
   elm_object_style_set(popup, "transparent");
   elm_object_text_set(popup,
                       _("<align=center>Would you like to save changes to document?<br>"
                         "Any unsaved changes will be lost."));

   btn = elm_button_add(popup);
   elm_object_text_set(btn, _("Save"));
   elm_object_part_content_set(popup, "button1",  btn);
   evas_object_smart_callback_add(btn, "clicked", _cb_save, ad);

   btn = elm_button_add(popup);
   elm_object_text_set(btn, _("Discard"));
   elm_object_part_content_set(popup, "button2", btn);
   evas_object_smart_callback_add(btn, "clicked", _cb_discard, ad);

   btn = elm_button_add(popup);
   elm_object_text_set(btn, _("Cancel"));
   elm_object_part_content_set(popup, "button3", btn);
   evas_object_smart_callback_add(btn, "clicked", _cb_cancel, ad);

   elm_popup_orient_set(popup, ELM_POPUP_ORIENT_CENTER);
   evas_object_show(popup);
}

static void
_warning_popup_close_cb(void *data, Evas_Object *obj, void *event_info)
{
   evas_object_del((Evas_Object *) data);
}

void
ui_alert_warning_popup(Evas_Object *parent, const char *msg)
{
   Evas_Object *popup, *btn;

   popup = elm_popup_add(parent);
   elm_object_part_text_set(popup, "title,text", _("Warning"));
   elm_object_text_set(popup, eina_slstr_printf("<align=center>%s.</>", msg));

   btn = elm_button_add(popup);
   elm_object_text_set(btn, _("Close"));
   evas_object_show(btn);
   evas_object_smart_callback_add(btn, "clicked", _warning_popup_close_cb, popup);
   elm_object_part_content_set(popup, "button1", btn);

   elm_popup_orient_set(popup, ELM_POPUP_ORIENT_CENTER);
   evas_object_show(popup);
}

